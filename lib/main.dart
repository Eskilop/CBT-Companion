import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:cbt_companion/model/entry.dart';
import 'package:cbt_companion/dbhelper.dart';
import 'package:flutter/gestures.dart';
import 'package:path/path.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:encrypt/encrypt.dart';

void main() => runApp(CBTApp());

class CBTApp extends StatelessWidget {
  // #docregion build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Color.fromARGB(255, 28, 37, 55),
        accentColor: Color.fromARGB(255, 0, 255, 173),

        // Define the default font family.
        fontFamily: 'Lato',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      title: 'CBT Companion',
      home: Entries(),
    );
  }
// #enddocregion build
}

class Entries extends StatefulWidget {
  @override
  EntriesState createState() => EntriesState();
}

class EntriesState extends State<Entries> {
  List<Entry> _entries = new List<Entry>();

  void toMd() async {
    String result = "# A-B-C Report for in date ${DateTime.now()} ";

    this._entries.forEach((e) {
      result += "## ${e.date}";
      result += "### Activating Event";
      result += "\n";
      result += e.activator;
      result += "\n";
      result += "### Belief";
      result += "\n";
      result += e.belief;
      result += "\n";
      result += "### Consequence ${e.consequence}";
      result += "\n";
      result += e.consequence;
      result += "\n";
      result += "---";
    });

    final directory = await getExternalStorageDirectory();
    final file = File('${directory.path}/report.md');
    await file.writeAsString(result);

    var share = Share.shareFiles(
        [file.path],
        text: "Report ${DateTime.now()}"
    );
  }

  void backupDb() async {
    Directory d = await getExternalStorageDirectory();
    String path = join(d.path, "data_" + DateTime.now().toString() + ".db");

    if (!d.existsSync()) {
      d.create();
    }

    var fL = await d.list().toList();

    fL.removeWhere((test) {
      return test.toString().contains("report.md");
    });

    if (fL.length > 2) {
      fL[0].deleteSync();
    }

    String databasesPath = await getDatabasesPath();
    File from = new File(join(databasesPath, 'data.db'));
    Uint8List bytes = from.readAsBytesSync();
    //ByteData.view(bytes.buffer);

    ByteData data = ByteData.view(bytes.buffer);
    List<int> bytesLst =
        bytes.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await new File(path).writeAsBytes(bytesLst);
  }

  void restoreDb() async {
    Directory d = await getExternalStorageDirectory();
    var fL = await d.list().toList();

    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'data.db');

    if (!d.existsSync()) {
      d.create();
      return; // no backup to restore
    }

    if (fL.length < 2) {
      return; // no backup to restore
    }

    File from = new File(fL[1].path);
    Uint8List bytes = from.readAsBytesSync();
    //ByteData.view(bytes.buffer);

    ByteData data = ByteData.view(bytes.buffer);
    List<int> bytesLst =
        bytes.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await new File(path).writeAsBytes(bytesLst);
  }

  void update() {
    final DBHelper dbh = DBHelper();

    Future<List> _entries = dbh.getAllEntries();
    _entries.then((entries) {
      setState(() {
        this._entries.clear();
        entries.forEach((e) {
          this._entries.add(Entry.fromMap(e));
        });
      });
    });
  }

  int delete(id) {
    final DBHelper dbh = DBHelper();
    dbh.deleteEntry(id).then((success) {
      return success;
    });
  }

  Widget _buildEntries() {
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              color: Color.fromARGB(150, 255, 255, 255),
            ),
        padding: new EdgeInsets.only(top: 0.0, right: 0.0, left: 0.0),
        itemCount: _entries.length,
        itemBuilder: (context, i) {
          return _buildRow(_entries[i]);
        });
  }

  Widget _buildRow(Entry e) {
    var title = DateTime.fromMillisecondsSinceEpoch(e.date).toString() +
        " • " +
        e.consequence;
    var sub = e.activator + " -> " + e.belief;
    return ListTile(
      title: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      subtitle: Text(sub),
      trailing: Icon(Icons.delete),
      onTap: () {
        delete(e.id);
        update();
      },
      isThreeLine: true,
    );
  }

  @override
  Widget build(BuildContext context) {

    update();

    backupDb();

    return Scaffold(
      appBar: AppBar(
        title: Text('CBT Companion'),
        actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              toMd();
            },
          ),
          IconButton(
            icon: Icon(Icons.info_outline),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  title: "Success",
                  description:
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                  buttonText: "Okay",
                ),
              );
            },
          )
        ],
      ),
      body: _buildEntries(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddEntry()),
          ).then((value) {
            update();
          });
        },
        tooltip: 'Add Entry',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class AddEntry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var db = DBHelper();
    final activatorController = TextEditingController();
    final beliefController = TextEditingController();
    final consequenceController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Event"),
      ),
      body: Column(
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              child: TextField(
                obscureText: false,
                controller: activatorController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Activator',
                ),
              )),
          Container(
              margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              child: TextField(
                obscureText: false,
                controller: beliefController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Belief',
                ),
              )),
          Container(
              margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              child: TextField(
                obscureText: false,
                controller: consequenceController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Consequence',
                ),
              )),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var e = new Entry(
              activatorController.text,
              beliefController.text,
              consequenceController.text,
              DateTime.now().millisecondsSinceEpoch);

          db.saveEntry(e).then((onValue) {}, onError: (error) {
            print("AddEntry Error: $error");
          });
          Navigator.of(context).pop(e);
        },
        tooltip: 'Done',
        child: Icon(Icons.done),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 70.0;
  static const double avatarPadding = avatarRadius + 20;

  static const Color primaryColor = Color.fromARGB(255, 28, 37, 55);
  static const Color accentColor = Color.fromARGB(255, 0, 255, 173);
}

class _LinkTextSpan extends TextSpan {
  _LinkTextSpan({TextStyle style, String url, String text})
      : super(
            style: style,
            text: text ?? url,
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                launch(url, forceSafariVC: false);
              });
}

class CustomDialog extends StatelessWidget {
  final String title, description, buttonText;
  final Image image;

  CustomDialog({
    @required this.title,
    @required this.description,
    @required this.buttonText,
    this.image,
  });

  dialogContent(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final TextStyle linkStyle =
        themeData.textTheme.body2.copyWith(color: themeData.accentColor);
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: Consts.avatarRadius + Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.grey[900],
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                "Made w/ ❤️ by Eskilop",
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 16.0),
              new RichText(
                text: new TextSpan(
                  children: [
                    new TextSpan(
                      text: 'Copyright © 2019-present Luca D\'Amato\n\n',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    _LinkTextSpan(
                        style: linkStyle,
                        url: 'https://www.eskilop.it/bio',
                        text: '@Eskilop'),
                  ],
                ),
              ),
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                  child: Text(buttonText),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: Consts.avatarPadding,
          right: Consts.avatarPadding,
          child: CircleAvatar(
            backgroundImage: NetworkImage(
                'https://secure.gravatar.com/avatar/7cf44f708e5e4578360eb8a971aaa8fe?s=512&d=identicon',),
            radius: Consts.avatarRadius,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }
}
