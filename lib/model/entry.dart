class Entry
{
  int _id;
  String activator;
  String belief;
  String consequence;
  int date;

  Entry(this.activator, this.belief, this.consequence, this.date);

  Entry.map(dynamic obj) {
    this._id = obj['id'];
    this.activator = obj['activator'];
    this.belief = obj['belief'];
    this.consequence = obj['consequence'];
    this.date = obj['date'];
  }

  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['activator'] = activator;
    map['belief'] = belief;
    map['consequence'] = consequence;
    map['date'] = date;
    return map;
  }

  Entry.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this.activator = map['activator'];
    this.belief = map['belief'];
    this.consequence = map['consequence'];
    this.date = map['date'];
  }

  @override
  String toString() {
    return '$date • $consequence | $activator -> $belief';
  }
}