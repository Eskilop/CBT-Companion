import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:cbt_companion/model/entry.dart';

class DBHelper {
  static final DBHelper _instance = new DBHelper.internal();

  factory DBHelper() => _instance;

  final String tableEntries = 'entries';
  final String columnId = 'id';
  final String columnActivator = 'activator';
  final String columnBelief = 'belief';
  final String columnConsequence = 'consequence';
  final String columnDate = 'date';

  static Database _db;

  DBHelper.internal();

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();

    return _db;
  }

  initDb() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'data.db');

    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $tableEntries('
            '$columnId INTEGER PRIMARY KEY AUTOINCREMENT,'
            '$columnActivator TEXT NOT NULL,'
            '$columnBelief TEXT NOT NULL,'
            '$columnConsequence TEXT NOT NULL,'
            '$columnDate TIMESTAMP NOT NULL)');
  }

  Future<int> saveEntry(Entry e) async {
    var dbClient = await db;
    var result = await dbClient.insert(tableEntries, e.toMap());
    return result;
  }

  Future<List> getAllEntries() async {
    var dbClient = await db;
    var result = await dbClient.query(tableEntries, columns: [columnId, columnActivator, columnBelief, columnConsequence, columnDate]);
    return result.toList();
  }

  Future<int> getCount() async {
    var dbClient = await db;
    var v = await dbClient.rawQuery('SELECT COUNT(*) FROM $tableEntries');
    return Sqflite.firstIntValue(v);
  }

  Future<Entry> getEntry(int id) async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(tableEntries,
        columns: [columnId, columnActivator, columnBelief, columnConsequence, columnDate],
        where: '$columnId = ?',
        whereArgs: [id]);

    if (result.length > 0) {
      return new Entry.fromMap(result.first);
    }

    return null;
  }

  Future<int> deleteEntry(int id) async {
    var dbClient = await db;
    var r = await dbClient.delete(tableEntries, where: '$columnId = ?', whereArgs: [id]);
    return r;
  }

  Future<int> updateEntry(Entry e) async {
    var dbClient = await db;
    var r = await dbClient.update(tableEntries, e.toMap(), where: "$columnId = ?", whereArgs: [e.id]);
    return r;
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}